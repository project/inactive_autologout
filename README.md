# Inactive Autologout

This module provides a site administrator the ability to log out
users after a specified time of inactivity. It provides the
customized form where site administrator can change the inactivity
timeout and also enable the settings on the the bases of role as
well.and it is very light-weighted also.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/inactive_autologout).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/inactive_autologout).

## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Configure Automated logout : Home >> Administration >> Configuration >> People
   (`/admin/config/people/autologoutsettings`)

## Maintainers

- Siva Karthik Reddy Papudippu - [sivakarthik229](https://www.drupal.org/u/sivakarthik229)
- Hari Venu - [harivenuv](https://www.drupal.org/u/harivenuv)
- dipikarani swaro - [dipikas](https://www.drupal.org/u/dipikas)
